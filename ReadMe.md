# Noughts and Crosses Rest service

This is a free implementation of a service emulating the game "Noughts and
Crosses" in nodejs, using express.


# ToDo

01 [ ] - Initialise the project
02 [ ] - Install express
03 [ ] - Install jest
04 [ ] - Prepare folder for tests
05 [ ] - Write functional tests which will test the http layer
06 [ ] - Write unit tests on-the-go
 


# Design the API 

> initial thoughts

```
   | method | endpoint     | parameters                | description                    | response 
01 | GET    | /game/list   | null                      | get a list of available games  | array of existing game names
02 | POST   | /game/new    | name[string,required]     | start a new game               | an object as { "token" : "someRandomString" }
03 | POST   | /game/join   | name[string,required]     | join an existing game          | an object with keys: {status, message, error, turn, data, winner}
04 | GET    | /game/status | null                      | get the current game status    | an object with keys: {status, message, error, turn, data, winner}
05 | POST   | /game/play   | row[1,2,3], column[1,2,3] | make a move in the board cell  | an object with keys: {status, message, error, turn, data, winner}


cell: 

|11|12|13|
|21|22|23|
|31|32|33|

status keys:

enum statuses = {"NEW", "PLAYING", "FINISHED"}
enum errors = {"NOT_YOUR_TURN", "GAME_FINISHED", "UNAUTHORIZED", "CELL_OCCUPIED", "CELL_UNEXISTENT"}
enum turn = {"NOUGHTS", "CROSSES"}
enum winner = {"NOUGHTS", "CROSSES"}
```

that's waht initally I thought, I probably can simplify it later
now, time to get lunch, so I pause the exercise here